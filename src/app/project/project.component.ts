import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Project } from '../projects/project.model';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  @Input() project: Project;
  @Output() statusUpdated = new EventEmitter<string>();
  @Output() projectDeleted = new EventEmitter<void>();

  status: string;
  constructor() { }

  ngOnInit() {
  }

  onUpdateStatus(newStatus: string) {
    this.status = newStatus;
    this.statusUpdated.emit(newStatus);
  }

  onDelete() {
    this.projectDeleted.emit();
  }

  getPrjStatus(){
    return {
      'active': this.project.status === 'active',
      'inactive': this.project.status === 'inactive',
      'critical': this.project.status === 'critical'

    }
  }
}
