import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { Project } from './project.model';

export class ProjectsService {
  private projects: Project[] = [
    { name: 'ISR N.O 112', description: 'Update dealer interface', status: 'active'},
    { name: 'ISR N.O 125', description: 'HINO signature change', status: 'active'},
    { name: 'ISR N.O 142', description: 'Update staff details', status: 'inactive'},
    { name: 'ISR N.O 122', description: 'Absolutely required to dive deep into Angular and all its features', status: 'critical'},
  ];

  loadProjects(): Observable<Project[]> {
    const prjLoader = Observable.create((observer: Observer<Project[]>) => {
      setTimeout(() => {
        observer.next(this.projects);
      }, 2000);
    });
    return prjLoader;
  }
}
